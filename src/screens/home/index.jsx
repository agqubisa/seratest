import {StyleSheet, Text, View} from 'react-native';
import React, {useEffect} from 'react';
import {black} from '~common';

const HomeScreen = () => {
  const onRefresh = async () => {
    // try {
    //   const result = await getList({params: {page: 1}});
    //   console.log(result);
    // } catch (error) {
    //   console.log('error : ', error);
    // }
  };

  useEffect(() => {
    onRefresh();
  }, []);

  return (
    <View>
      <Text style={{color: black}}>HomeScreen</Text>
    </View>
  );
};

export default HomeScreen;

const styles = StyleSheet.create({});
